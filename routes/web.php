<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes(['register'=>false]);

Route::get('/home', 'HomeController@index')->name('home');


/****************Admin Routes Start ****************/
Route::group(['prefix' => 'admin', 'middleware' => ['auth', 'admin']], function(){

    Route::get('/', function(){
        return view('admin.dashboard');
    })->name('admin');

    Route::resource('banner','BannerController')->except('show');
    Route::resource('category','CategoryController');
    Route::resource('products','ProductController');

    ROute::post('/category/child','CategoryController@getChildCats')->name('get-child');

    Route::get('file-manager', function(){
        return view('admin.file-manager');
    });

});
/****************Admin Routes End ****************/



/****************Vendor Routes Start ****************/
Route::group(['prefix' => 'vendor', 'middleware' => ['auth', 'vendor']], function(){

    Route::get('/', 'HomeController@vendor')->name('vendor');

});
/****************Vendor Routes End ****************/


/****************Customer Routes Start ****************/
Route::group(['prefix' => 'customer', 'middleware' => ['auth', 'customer']], function(){

    Route::get('/', function(){
        return view('home');
    })->name('customer');

});
/****************Customer Routes End ****************/
