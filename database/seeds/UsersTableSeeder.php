<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;
use App\User;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $users = array(
                array(
                    'name' => 'Admin Bcom.com',
                    'email' => 'admin@bcom.com',
                    'password' => Hash::make('admin123'),
                    'status' => 'verified',
                    'role' => 'admin'
                ),
                array(
                    'name' => 'Vendor Bcom.com',
                    'email' => 'vendor@bcom.com',
                    'password' => Hash::make('vendor123'),
                    'status' => 'verified',
                    'role' => 'vendor'
                ),
                array(
                    'name' => 'Customer Bcom.com',
                    'email' => 'customer@bcom.com',
                    'password' => Hash::make('customer123'),
                    'status' => 'verified',
                    'role' => 'customer'
                )
        );

        foreach($users as $user_data){
            $user = User::where('email', $user_data['email'])->first();
            if(!$user){
                $user = new User();
                $user->fill($user_data);
                $user->save();              // INSERT
            }
        }
    }
}
