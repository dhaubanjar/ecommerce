<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Banner extends Model
{
    protected $fillable = ['title','thumbnail','link','image','status','added_by'];

    public function getRules(){
        return [
          'title' => ['required', 'string'],
          'link' => 'required|url',
          'image' => 'required|image|max:6000',
          'status' => 'required|in:active,inactive'

        ];
    }

    public function getUpdateRules(){
        return [
          'title' => ['required', 'string'],
          'link' => 'required|url',
          'image' => 'sometimes|image|max:6000',
          'status' => 'required|in:active,inactive'

        ];
    }
}

