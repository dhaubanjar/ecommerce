<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;

class Category extends Model
{
    protected $fillable = ['title','slug','summary','is_parent','parent_id','image','status','added_by'];

    public function getRules($act = 'add'){
        $rules = [
            'title' => 'required|string|unique:categories,title',
            'summary' => 'nullable|string',
            'is_parent' => 'sometimes|in:1',
            'parent_id' => 'sometimes|exists:categories,id',
            'image' => 'sometimes|image|max:3000',
            'status' => 'required|in:active,inactive',
        ];
        if ($act != 'add'){
            $rules['title'] = 'required|string';
        }

        return $rules;
    }

    public function getSlug($title){
        $slug = Str::slug($title);

//        validating slug if it exists or not

        $exists = $this->where('slug',$slug)->first();
        if ($exists){
            $slug .= date('ymdhis');
        }
        return $slug;

    }

    public function parent_info(){
        return $this->hasOne('App\Models\Category', 'id', 'parent_id');
    }

    public function getAllCategories(){
        return $this->with('parent_info')->orderBy('id','DESC')->get();
    }

    public function getAllParents(){
//        SELECT * FROM categories WHERE is+parent=1
        //id => title so use pluck()
        return $this->where('is_parent',1)->orderBy('title','ASC')->pluck('title','id');

    }

    //for delete function

    public function getChildCatId($parent_id){
        return $this->where('parent_id', $parent_id)->pluck('id');
    }

}
