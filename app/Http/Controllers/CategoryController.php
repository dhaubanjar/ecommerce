<?php

namespace App\Http\Controllers;

use App\Models\Category;
use Illuminate\Http\Request;

class CategoryController extends Controller
{
    protected $category = null;

    public function __construct(Category $_category)
    {
        $this->category = $_category;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $all_categories = $this->category->getAllCategories();
        return view('admin.category')->with('category_list',$all_categories);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $this->category = $this->category->getAllParents();
        return view('admin.category-form')->with('parent_cats', $this->category);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $rules = $this->category->getRules();
        $request->validate($rules);

        $data = $request->all();
        $data['added_by'] = $request->user()->id;
        $data['slug'] = $this->category->getSlug($request->title);



        if ($request->has('image')){
            $category_image = uploadImage($request->image, 'category', '200x200');
            if($category_image){
                $data['image'] = $category_image;
            }
        }

//if child category, set default value of is_parent to 0.

        $data['is_parent']=$request->input('is_parent', 0);
        $data['parent_id'] = (isset($request->is_parent)) ?  null : $request->parent_id ;

        $this->category->fill($data);
        $success = $this->category->save();
        if($success){
            $request->session()->flash('success','Category added successfully');
        }else{
            $request->session()->flash('error','Error! There was a problem adding category');

        }

        return redirect()->route('category.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = $this->category->find($id);
        if(!$data){
            request()->session()->flash('error','Category not found');
            return redirect()->back();
        }

        $this->category = $this->category->getAllParents();
        return view('admin.category-form')
            ->with('category_data',$data)
            ->with('parent_cats',$this->category);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->category = $this->category->find($id);
        if(!$this->category){
            request()->session()->flash('error','Category not found');
            return redirect()->back();
        }
        $rules = $this->category->getRules('update');
        $request->validate($rules);

        $data = $request->all();

        if ($request->has('image')){
            $category_image = uploadImage($request->image, 'category', '200x200');
            if($category_image){
                $data['image'] = $category_image;
            }
        }

//if child category, set default value of is_parent to 0.

        $data['is_parent']=$request->input('is_parent', 0);
        $data['parent_id'] = (isset($request->is_parent)) ?  null : $request->parent_id ;

        $this->category->fill($data);
        $old_parent = $this->category->is_parent;
        $success = $this->category->save();
        if($success){

            if ($old_parent){
                $this->category->where('parent_id', $this->category->id)->update(['parent_id' =>$data['parent_id']]);
            }
            $request->session()->flash('success','Category updated successfully');
        }else{
            $request->session()->flash('error','Error! There was a problem updating category');

        }

        return redirect()->route('category.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //fetching ID
        $this->category = $this->category->find($id);

        //checking if the id exists or not
        if (!$this->category){
            request()->session()->flash('error','Category not found');
            return redirect()->back();
        }

        //storing information of image to use after deleting original
        $image = $this->category->image;
        $child_id = $this->category->getChildCatId($id);
        $del = $this->category->delete();

        //shifting child to parent category after  deleting parent id
        if ($del){
            //UPDATE categories SET is_parent = 1 where id = 2 OR id = 3
            //UPDATE categories SET is_parent = 1 where id IN (2,3)

            if ($child_id->count() > 0){
                $this->category->whereIn('id',$child_id)->update(['is_parent' => 1]);
            }

            if(!empty($image) && file_exists(public_path().'/uploads/category/'.$image)){
                unlink(public_path().'/uploads/category/'.$image);
                unlink(public_path().'/uploads/category/Thumb-'.$image);
            }

            request()->session()->flash('success','Category Deleted Successfully.');
        }else{
            request()->session()->flash('error','There was a problem deleting category');
        }
        return redirect()->route('category.index');
    }

    public function getChildCats(Request $request){
        $data = $this->category->where('parent_id',$request->cat_id)->pluck('title','id');
        if($data->count()){
            return response()->json(['status'=> true, 'data'=>$data]);
        }else{
            return response()->json(['status'=> false, 'data'=>null]);
        }
    }
}
