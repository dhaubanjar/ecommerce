<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');

    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index(Request $request)
    {
        // $this->middleware('auth');
        // dd($request->user()->role);
        // logic to redirect =>
        return redirect()->route($request->user()->role);

        /*echo $request->user()->role;
        return view('home');*/
    }


    public function vendor(Request $request){
        if($request->user()->role != 'vendor'){
            $request->session()->flash('status','You are not vendor');
            return redirect()->route($request->user()->role);
        }

        return view('home');
    }
}
