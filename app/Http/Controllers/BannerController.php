<?php

namespace App\Http\Controllers;

use App\Models\Banner;
use Illuminate\Http\Request;



class BannerController extends Controller
{
    protected $banner = null;
//creating a global object of model
    public function __construct(Banner $banner)
    {
        $this->banner = $banner;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $this->banner = $this->banner->orderBy('id','desc')->get();
        return view('admin.banner')->with('banner_list',$this->banner);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.banner-form');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $rules = $this->banner->getRules();
        $request ->validate($rules);
        $data = $request->all();

        if ($request->has('image')){
            $banner_image = uploadImage($request->image, 'banner', '1920x930');
            if($banner_image){
                $data['image'] = $banner_image;
            }
        }

        $this->banner -> fill($data);
        $success = $this->banner->save();
        if($success){
            $request->session()->flash('success','Banner added successfully.');
        }else{
            $request->session()->flash('error','Sorry! There was problem adding banner');
        }

        return redirect()->route('banner.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $this->banner = $this->banner->find($id);
        if(!$this->banner){
            request()->session()->flash('error','Banner not found');
            return redirect()->back();
        }

        return view('admin.banner-form')->with('banner_data', $this->banner);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $rules = $this->banner->getUpdateRules();
        $request ->validate($rules);
        $data = $request->all();

        $this->banner = $this->banner->find($id);
        if(!$this->banner) {
            request()->session()->flash('error', 'Banner not found');
            return redirect()->back();
        }

        if ($request->has('image')){
            $banner_image = uploadImage($request->image, 'banner', '192x930');
            if($banner_image){
                $data['image'] = $banner_image;

                if (file_exists(public_path().'/uploads/banner/'.$this->banner->image)){
                       unlink(public_path().'/uploads/banner/'.$this->banner->image);
                       unlink(public_path().'/uploads/banner/Thumb-'.$this->banner->image);
                }

            }
        }

        $this->banner->fill($data);
        $success = $this->banner->save();
        if($success){
            $request->session()->flash('success','Banner Updated Successfully.');
        }else{
            $request->session()->flash('error','Sorry! There was a problem while updating banner.');
        }

        return redirect()->route('banner.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $this->banner = $this->banner->find($id);
        if(!$this->banner){
            request()->session()->flash('error','Banner not found');
            return redirect()->back();
        }

        $image = $this->banner->image;
        $success = $this->banner->delete();
        if($success){
            if($image != null && file_exists(public_path().'/uploads/banner/'.$image)){
                unlink(public_path().'/uploads/banner/'.$image);
                unlink(public_path().'/uploads/banner/Thumb-'.$image);
            }
            request()->session()->flash('success','Banner deleted successfully.');
        }else{
            request()->session()->flash('error','Problem deleting banner');

        }

        return redirect()->back();
    }
}
