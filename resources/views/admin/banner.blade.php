@extends('admin.layouts.admin')
@section('content')

  <div class="page-content fade-in-up">

    <div class="row">
      <div class="col-lg-12">
        <div class="ibox">
          <div class="ibox-head">
            <div class="ibox-title">Banner List</div>
            <div class="ibox-tools">
              <a href="{{route('banner.create')}}" class="btn btn-primary"><i class="fa fa-plus">Add New</i> </a>
            </div>
          </div>
          <div class="ibox-body">
            <table class="table table-striped table-hover">
              <thead>
              <tr>
                <th>Title</th>
                <th>Thumbnail</th>
                <th>Link</th>
                <th>Status</th>
                <th>Action</th>
              </tr>
              </thead>
              <tbody>
              @if($banner_list)
                @foreach($banner_list as $key => $value)
                  <tr>
                    <td>{{$key + 1}}</td>
                    <td>
                      @if($value->image != null && file_exists(public_path().'/uploads/banner/Thumb-'.$value->image))
                        <img src="{{asset('/uploads/banner/Thumb-').$value->image}}" alt="" class="img img-responsive img-thumbnail">
                      @endif
                    </td>
                    <td>
                      <a href="{{$value->link}}" target="_banner">{{$value->link}}</a>
                    </td>
                    <td>{{$value->status}}</td>
                    <td>
                      <a href="{{route('banner.edit', $value->id)}}" class="btn btn-success" style="border-radius: 50%; margin-bottom: 2px">
                        <i class="fa fa-pencil"></i>
                      </a>

                      {{ Form::open(['url' => route('banner.destroy', $value->id), 'class' => 'form', 'method'=>'delete', 'onsubmit' => "return confirm('Are you sure you want to delete this banner?')"]) }}
                      {{ Form::button("<i class='fa fa-trash'></i>",['class' => 'btn btn-danger', 'style' => 'border-radius:50%', 'type' => 'submit']) }}
                      {{ Form::close() }}
                    </td>
                  </tr>
                @endforeach
              @endif
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>


  </div>
@endsection
