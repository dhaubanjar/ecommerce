<!-- START SIDEBAR-->
<nav class="page-sidebar" id="sidebar">
  <div id="sidebar-collapse">
    <div class="admin-block d-flex">
      <div>
        <img src="{{ asset('assets/admin/img/admin-avatar.png') }}" width="45px" />
      </div>
      <div class="admin-info">
        <div class="font-strong">James Brown</div><small>Administrator</small></div>
    </div>
    <ul class="side-menu metismenu">
      <li>
        <a class="active" href="index.html"><i class="sidebar-item-icon fa fa-th-large"></i>
          <span class="nav-label">Dashboard</span>
        </a>
      </li>

      <li class="heading">FEATURES</li>

      <li>
        <a href="{{route('banner.index')}}"><i class="sidebar-item-icon fa fa-image"></i>
          <span class="nav-label">Banner Management</span>
        </a>
      </li>
      <li>
        <a href="{{route('category.index')}}"><i class="sidebar-item-icon fa fa-sitemap"></i>
          <span class="nav-label">Category Management</span>
        </a>
      </li>
      <li>
        <a href="icons.html"><i class="sidebar-item-icon fa fa-image"></i>
          <span class="nav-label">Media Management</span>
        </a>
      </li>
      <li>
        <a href="{{ route('products.index') }}"><i class="sidebar-item-icon fa fa-shopping-basket"></i>
          <span class="nav-label">Product Management</span>
        </a>
      </li>
      <li>
        <a href="icons.html"><i class="sidebar-item-icon fa fa-shopping-cart"></i>
          <span class="nav-label">Order Management</span>
        </a>
      </li>
      <li>
        <a href="icons.html"><i class="sidebar-item-icon fa fa-users"></i>
          <span class="nav-label">Users Management</span>
        </a>
      </li>
      <li>
        <a href="icons.html"><i class="sidebar-item-icon fa fa-dollar"></i>
          <span class="nav-label">Ads Management</span>
        </a>
      </li>
      <li>
        <a href="icons.html"><i class="sidebar-item-icon fa fa-file"></i>
          <span class="nav-label">Pages Management</span>
        </a>
      </li>
      <li>
        <a href="icons.html"><i class="sidebar-item-icon fa fa-comments"></i>
          <span class="nav-label">Review Management</span>
        </a>
      </li>
    </ul>
  </div>
</nav>
<!-- END SIDEBAR-->