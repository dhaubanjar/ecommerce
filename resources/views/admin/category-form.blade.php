@extends('admin.layouts.admin')
@section('content')
    <div class="page-content fade-in-up">
        <div class="row">
            <div class="col-lg-12">
                <div class="ibox">
                    <div class="ibox-head">
                        <div class="ibox-title">Category {{ isset($category_data) ? 'Update' : 'Add' }}</div>
                    </div>
                    <div class="ibox-body">
                        @if(isset($category_data))
                            {{Form::open(['url'=>route('category.update', $category_data->id), 'class'=>'form', 'id'=>'category_add', 'files'=>true, 'method' => 'patch'])}}

                        @else

                            {{Form::open(['url'=>route('category.store'), 'class'=>'form', 'id'=>'category_add', 'files'=>true])}}
                        @endif

                        <div class="form-group row">
                            {{ Form::label('title','Title:', ['class'=>'col-sm-3']) }}
                            <div class="col-sm-9">
                                {{ Form::text('title', @$category_data->title, ['class'=>'form-control form-control-sm'.($errors->has('title') ? 'is-invalid' : ''), 'id'=>'title', 'required'=>true, ]) }}
                                @error('title')
                                <span class="invalid-feedback">
                                    {{$message}}
                                </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            {{ Form::label('summary','Summary:', ['class'=>'col-sm-3']) }}
                            <div class="col-sm-9">
                                {{ Form::textarea('summary', @$category_data->summary, ['class'=>'form-control form-control-sm'.($errors->has('summary') ? 'is-invalid' : ''), 'id'=>'summary', 'required'=>false, 'placeholder' =>'Enter the summary', 'rows' => 5, 'style' => 'resize:none']) }}
                                @error('summary')
                                <span class="invalid-feedback">
                                    {{$message}}
                                </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            {{ Form::label('is_parent','Is Parent:', ['class'=>'col-sm-3']) }}
                            <div class="col-sm-1">
                                {{ Form::checkbox('is_parent', 1, @$category_data->is_parent, ['class'=>'form-control form-control-sm'.($errors->has('summary') ? 'is-invalid' : ''), 'id'=>'is_parent']) }}
                                @error('summary')
                                <span class="invalid-feedback">
                                    {{$message}}
                                </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row {{@$category_data->is_parent ? 'd-none' : ''}}" id="parent_id_div">
                            {{ Form::label('parent_id','Parent Category:', ['class'=>'col-sm-3']) }}
                            <div class="col-sm-9">
                                {{ Form::select('parent_id', $parent_cats, @$category_data->parent_id, ['class'=>'form-control form-control-sm'.($errors->has('status') ? 'is-invalid' : ''), 'id'=>'parent_id', 'required'=>false ]) }}

                                @error('parent_id')
                                <span class="invalid-feedback">
                                    {{$message}}
                                </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            {{ Form::label('status','Status:', ['class'=>'col-sm-3']) }}
                            <div class="col-sm-9">
                                {{ Form::select('status',['active'=>'Active','inactive'=>'Inactive'], @$category_data->status,['class'=>'form-control form-control-sm'.($errors->has('status') ? 'is-invalid' : ''), 'id'=>'status', 'required'=>true, ]) }}

                                @error('status')
                                <span class="invalid-feedback">
                                    {{$message}}
                                </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            {{ Form::label('image','Image:', ['class'=>'col-sm-3']) }}
                            <div class="col-sm-4">
                                {{ Form::file('image', ['class'=>'form-control-file'.($errors->has('image') ? 'is-invalid' : ''), 'id'=>'image', 'required'=>(isset($category_data) ? false : true), 'accept'=>'image/*']) }}
                                @error('image')
                                <span class="invalid-feedback">
                                    {{$message}}
                                </span>
                                @enderror
                            </div>
                            <div class="col-sm-4">
                                @if(isset($category_data) && $category_data->image != null && file_exists(public_path().'/uploads/category/Thumb-'.$category_data->image))
                                    <img src="{{asset('/uploads/category/Thumb-').$category_data->image}}" alt="" class="img img-responsive img-thumbnail">
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            {{ Form::label('','', ['class'=>'col-sm-3']) }}
                            <div class="col-sm-9">
                                {{ Form::button('<i class="fa fa-trash"></i> Reset',['class'=>'btn btn-danger', 'type'=>'reset']) }}
                                {{ Form::button('<i class="fa fa-send"></i> Submit',['class'=>'btn btn-success', 'type'=>'submit']) }}
                            </div>
                        </div>
                        {{Form::close()}}
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')

    <script>
        $('#is_parent').change(function () {
            var is_checked = $('#is_parent').prop('checked');
            if (is_checked){
                $('#parent_id_div').addClass('d-none');

            } else {
                $('#parent_id_div').removeClass('d-none');
            }
        })
    </script>
    @endsection
