@extends('admin.layouts.admin')
@section('content')

  <div class="page-content fade-in-up">

    <div class="row">
      <div class="col-lg-12">
        <div class="ibox">
          <div class="ibox-head">
            <div class="ibox-title">Category List</div>
            <div class="ibox-tools">
              <a href="{{route('category.create')}}" class="btn btn-primary"><i class="fa fa-plus">Add New</i> </a>
            </div>
          </div>
          <div class="ibox-body">
            <table class="table table-striped table-hover">
              <thead>
              <tr>
                <th>S.N</th>
                <th>Title</th>
                <th>Thumbnail</th>
                <th>Slug</th>
                <th>Summary</th>
                <th>Is Parent?</th>
                <th>Parent</th>
                <th>Status</th>
                <th>Action</th>
              </tr>
              </thead>
              <tbody>
              @if($category_list)
                @foreach($category_list as $key => $value)
                  <tr>
                    <td>{{$key + 1}}</td>
                    <td>{{$value->title}}</td>
                    <td>
                      @if($value->image != null && file_exists(public_path().'/uploads/category/Thumb-'.$value->image))
                        <img src="{{asset('/uploads/category/Thumb-').$value->image}}" alt="" class="img img-responsive img-thumbnail">
                      @endif
                    </td>
                    <td>{{$value->slug}}</td>
                    <td>
                      <a href="{{$value->summary}}" target="_category">{{$value->summary}}</a>
                    </td>
                    <td>{{$value->is_parent == 1 ? "Yes" : "No"}}</td>
                    <td>{{$value->parent_id == 0 ? "N/a" : $value->parent_info['title']}}</td>
                    <td>{{$value->status}}</td>
                    <td>
                      <a href="{{route('category.edit', $value->id)}}" class="btn btn-success" style="border-radius: 50%; margin-bottom: 2px"><i class="fa fa-pencil"></i>
                      </a>

                      {{ Form::open(['url' => route('category.destroy', $value->id), 'class' => 'form', 'method'=>'delete', 'onsubmit' => "return confirm('Are you sure you want to delete this category?')"]) }}
                      {{ Form::button("<i class='fa fa-trash'></i>",['class' => 'btn btn-danger', 'style' => 'border-radius:50%', 'type' => 'submit']) }}
                      {{ Form::close() }}
                    </td>
                  </tr>
                @endforeach
              @endif
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>


  </div>
@endsection
