@extends('admin.layouts.admin')

@section('styles')
  <link rel="stylesheet" href="{{asset('assets/admin/css/select2.css')}}">
  <link rel="stylesheet" href="{{asset('assets/admin/css/select2.css')}}">
@endsection

@section('content')
  <div class="page-content fade-in-up">
    <div class="row">
      <div class="col-lg-12">
        <div class="ibox">
          <div class="ibox-head">
            <div class="ibox-title">Product {{ isset($product_data) ? 'Update' : 'Add' }}</div>
          </div>
          <div class="ibox-body">
            @if(isset($product_data))
              {{Form::open(['url'=>route('product.update', $product_data->id), 'class'=>'form', 'id'=>'product_add', 'files'=>true, 'method' => 'patch'])}}

            @else

              {{Form::open(['url'=>route('products.store'), 'class'=>'form', 'id'=>'product_add', 'files'=>true])}}
            @endif

            <div class="form-group row">
              {{ Form::label('title','Title:', ['class'=>'col-sm-3']) }}
              <div class="col-sm-9">
                {{ Form::text('title', @$product_data->title, ['class'=>'form-control form-control-sm'.($errors->has('title') ? 'is-invalid' : ''), 'id'=>'title', 'required'=>true, ]) }}
                @error('title')
                <span class="invalid-feedback">
                                    {{$message}}
                                </span>
                @enderror
              </div>
            </div>

            <div class="form-group row">
              {{ Form::label('summary','Summary:', ['class'=>'col-sm-3']) }}
              <div class="col-sm-9">
                {{ Form::textarea('summary', @$product_data->summary, ['class'=>'form-control form-control-sm'.($errors->has('summary') ? 'is-invalid' : ''), 'id'=>'summary', 'required'=>false, 'placeholder' =>'Enter the summary', 'rows' => 5, 'style' => 'resize:none']) }}
                @error('summary')
                <span class="invalid-feedback">
                                    {{$message}}
                                </span>
                @enderror
              </div>
            </div>

            <div class="form-group row">
              {{ Form::label('description','Description:', ['class'=>'col-sm-3']) }}
              <div class="col-sm-9">
                {{ Form::textarea('description', @$product_data->description, ['class'=>'form-control form-control-sm'.($errors->has('description') ? 'is-invalid' : ''), 'id'=>'description', 'placeholder' =>'Enter the description', 'rows' => 5, 'style' => 'resize:none']) }}
                @error('description')
                <span class="invalid-feedback">
                                    {{$message}}
                                </span>
                @enderror
              </div>
            </div>

            <div class="form-group row">
              {{ Form::label('cat_id','Category:', ['class'=>'col-sm-3']) }}
              <div class="col-sm-9">
                {{ Form::select('cat_id', $parent_cats, @$product_data->cat_id, ['class'=>'form-control form-control-sm category_select_2'.($errors->has('cat_id') ? 'is-invalid' : ''), 'id'=>'cat_id','placeholder'=>'Select One Category']) }}
                @error('cat_id')
                <span class="invalid-feedback">
                                    {{$message}}
                                </span>
                @enderror
              </div>
            </div>

            <div class="form-group row d-none" id="child_cat_div">
              {{ Form::label('sub_cat_id','Sub-category:', ['class'=>'col-sm-3']) }}
              <div class="col-sm-9">
                {{ Form::select('sub_cat_id', [], @$product_data->sub_cat_id, ['class'=>'form-control form-control-sm '.($errors->has('sub_cat_id') ? 'is-invalid' : ''), 'id'=>'sub_cat_id']) }}
                @error('sub_cat_id')
                <span class="invalid-feedback">
                                    {{$message}}
                                </span>
                @enderror
              </div>
            </div>

              <div class="form-group row">
                {{ Form::label('price','Price (Rs):', ['class'=>'col-sm-3']) }}
                <div class="col-sm-9">
                  {{ Form::number('price', @$product_data->price, ['class'=>'form-control form-control-sm'.($errors->has('price') ? 'is-invalid' : ''), 'id'=>'price', 'required'=>true, 'placeholder' => "Enter price",'min'=>"10" ]) }}
                  @error('price')
                  <span class="invalid-feedback">
                                    {{$message}}
                                </span>
                  @enderror
                </div>
              </div>

              <div class="form-group row">
                {{ Form::label('discount','Discount (%):', ['class'=>'col-sm-3']) }}
                <div class="col-sm-9">
                  {{ Form::number('discount', @$product_data->discount, ['class'=>'form-control form-control-sm'.($errors->has('discount') ? 'is-invalid' : ''), 'id'=>'discount', 'required'=>false, 'placeholder' => "Enter discount",'min'=>"0", 'max'=>'95' ]) }}
                  @error('discount')
                  <span class="invalid-feedback">
                                    {{$message}}
                                </span>
                  @enderror
                </div>
              </div>

              <div class="form-group row">
                {{ Form::label('stock','Stock:', ['class'=>'col-sm-3']) }}
                <div class="col-sm-9">
                  {{ Form::number('stock', @$product_data->stock, ['class'=>'form-control form-control-sm'.($errors->has('stock') ? 'is-invalid' : ''), 'id'=>'stock', 'required'=>false, 'placeholder' => "Enter stock",'min'=>"0" ]) }}
                  @error('stock')
                  <span class="invalid-feedback">
                                    {{$message}}
                                </span>
                  @enderror
                </div>
              </div>

              <div class="form-group row">
                {{ Form::label('brand','Brand:', ['class'=>'col-sm-3']) }}
                <div class="col-sm-9">
                  {{ Form::text('brand', @$product_data->brand, ['class'=>'form-control form-control-sm'.($errors->has('brand') ? 'is-invalid' : ''), 'id'=>'brand', 'required'=>false, 'placeholder' => "Enter brand" ]) }}
                  @error('brand')
                  <span class="invalid-feedback">
                                    {{$message}}
                                </span>
                  @enderror
                </div>
              </div>

              <div class="form-group row">
                {{ Form::label('is_featured','Featured Product:', ['class'=>'col-sm-3']) }}
                <div class="col-sm-9">
                  {{ Form::checkbox('is_featured', 1, @$product_data->is_featured, ['class'=>''.($errors->has('is_featured') ? 'is-invalid' : ''), 'id'=>'is_featured' ]) }} Yes
                  @error('is_featured')
                  <span class="invalid-feedback">
                                    {{$message}}
                                </span>
                  @enderror
                </div>
              </div>

              <div class="form-group row">
                {{ Form::label('vendor_id','Vendor:', ['class'=>'col-sm-3']) }}
                <div class="col-sm-9">
                  {{ Form::select('vendor_id', $vendors, @$product_data->vendor_id, ['class'=>'form-control form-control-sm category_select_2 '.($errors->has('vendor_id') ? 'is-invalid' : ''), 'id'=>'vendor_id','placeholder'=>'Select One Vendor']) }}
                  @error('vendor_id')
                  <span class="invalid-feedback">
                                    {{$message}}
                                </span>
                  @enderror
                </div>
              </div>


              <div class="form-group row">
              {{ Form::label('status','Status:', ['class'=>'col-sm-3']) }}
              <div class="col-sm-9">
                {{ Form::select('status',['active'=>'Active','inactive'=>'Inactive'], @$product_data->status,['class'=>'form-control form-control-sm'.($errors->has('status') ? 'is-invalid' : ''), 'id'=>'status', 'required'=>true, ]) }}

                @error('status')
                <span class="invalid-feedback">
                                    {{$message}}
                                </span>
                @enderror
              </div>
            </div>

            <div class="form-group row">
              {{ Form::label('image','Image:', ['class'=>'col-sm-3']) }}
              <div class="col-sm-4">
                {{ Form::file('image', ['class'=>'form-control-file'.($errors->has('image') ? 'is-invalid' : ''), 'id'=>'image', 'required'=>(isset($product_data) ? false : true), 'accept'=>'image/*']) }}
                @error('image')
                <span class="invalid-feedback">
                                    {{$message}}
                                </span>
                @enderror
              </div>
              <div class="col-sm-4">
                @if(isset($product_data) && $product_data->image != null && file_exists(public_path().'/uploads/product/Thumb-'.$product_data->image))
                  <img src="{{asset('/uploads/product/Thumb-').$product_data->image}}" alt="" class="img img-responsive img-thumbnail">
                @endif
              </div>
            </div>

            <div class="form-group row">
              {{ Form::label('','', ['class'=>'col-sm-3']) }}
              <div class="col-sm-9">
                {{ Form::button('<i class="fa fa-trash"></i> Reset',['class'=>'btn btn-danger', 'type'=>'reset']) }}
                {{ Form::button('<i class="fa fa-send"></i> Submit',['class'=>'btn btn-success', 'type'=>'submit']) }}
              </div>
            </div>
            {{Form::close()}}
          </div>
        </div>
      </div>
    </div>
  </div>
@endsection

@section('scripts')
  <script src="{{asset('assets/admin/js/select2.min.js')}}"></script>
  <script>
      $(document).ready(function() {
          $('#cat_id').select2();
          $('#sub_cat_id').select2({width: "100%"});
      });
  </script>
  <script>
      $('#is_parent').change(function (e) {
          var is_checked = $('#is_parent').prop('checked');
          if (is_checked){
              $('#parent_id_div').addClass('d-none');

          } else {
              $('#parent_id_div').removeClass('d-none');
          }
      });

      $('#cat_id').change(function (e) {
          var cat_id = $(this).val();
          //ajax
          $.ajax({
              url: "{{route('get-child')}}",
              type: "post",
              data: {
                  _token: "{{csrf_token()}}",
                  cat_id: cat_id
              },
              success: function (response) {
                  if(typeof (response) != 'object'){
                      response = $.parseJSON(response);
                  }
                  var html_option = '<option value="" selected>Select Any One</option>';
                  if(response.status){
                      $('#child_cat_div').removeClass('d-none');

                      $.each(response.data, function (key, value) {
                          html_option += "<option value='" +key+"'>"+value+"</option>";
                      });
                  }else{
                      $('#child_cat_div').addClass('d-none')
                  }
                  $('#sub_cat_id').html(html_option);
              }

          });
      });
  </script>
@endsection
