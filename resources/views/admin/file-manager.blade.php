@extends('admin.layouts.admin')
@section('content')
  <div class="page-content fade-in-up">
    <div class="row">
      <div class="col-lg-12">
        <div class="ibox">
          <div class="ibox-head">
            <div class="ibox-title">Media Manager</div>
          </div>
          <div class="ibox-body">
            <div class="input-group">
                 <span class="input-group-btn">
                   <a id="lfm" data-input="thumbnail" data-preview="holder" class="btn btn-primary">
                     <i class="fa fa-picture-o"></i> Choose
                   </a>
                 </span>
              <input id="thumbnail" class="form-control" type="text" name="filepath">
            </div>
            <div class="row">
              <div id="holder" style="margin-top:15px;max-height:100px;"></div>
            </div>
          </div>
        </div>
      </div>
    </div>
    @endsection

    @section('scripts')
      <script src={{asset('vendor/laravel-filemanager/js/lfm.js')}}></script>
      <script>$('#lfm').filemanager('image');</script>
@endsection