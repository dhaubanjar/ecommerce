<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width initial-scale=1.0">
    <title>{{config('app.name', 'Admin BCOM')}} | {{config('app.slogan')}} | Dashboard</title>
    <!-- GLOBAL MAINLY STYLES-->
    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" rel="stylesheet" />
    <link href="{{ asset('assets/admin/vendor/font-awesome/css/font-awesome.min.css') }}" rel="stylesheet" />

    <link href="{{ asset('assets/admin/vendor/themify-icons/css/themify-icons.css') }}" rel="stylesheet" />
    <!-- THEME STYLES-->
    <link href="{{ asset('assets/admin/css/main.min.css') }}" rel="stylesheet" />
    <!-- PAGE LEVEL STYLES-->

    @yield('styles')
</head>

<body class="fixed-navbar">
<div class="page-wrapper">
    {{-- Header partials--}}
    @include('admin.partials.top-nav')

    {{-- SideBar partials--}}
    @include('admin.partials.sidebar')


    <div class="content-wrapper">

        <!-- START PAGE CONTENT-->
    {{-- Flash messages --}}
    @include('admin.partials.notifications')

    @yield('content')

    <!-- END PAGE CONTENT-->
        <footer class="page-footer">
            <div class="font-13">{{ date('Y') }}<b>&nbsp; &copy;Dhaubanjar</b> - All rights reserved.</div>
            <div class="to-top"><i class="fa fa-angle-double-up"></i></div>
        </footer>
    </div>
</div>

<!-- BEGIN PAGE BACKDROPS-->
<div class="sidenav-backdrop backdrop"></div>
<div class="preloader-backdrop">
    <div class="page-preloader">Loading</div>
</div>
<!-- END PAGE BACKDROPS-->
<!-- CORE PLUGINS-->
<script src="{{ asset('assets/admin/vendor/jquery/dist/jquery.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('assets/admin/vendor/popper.js/dist/umd/popper.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('assets/admin/vendor/bootstrap/dist/js/bootstrap.min.js') }}" type="text/javascript"></script>

<script src="{{ asset('assets/admin/vendor/metisMenu/dist/metisMenu.min.js') }}" type="text/javascript"></script>

<script src="{{ asset('assets/admin/vendor/jquery-slimscroll/jquery.slimscroll.min.js') }}" type="text/javascript"></script>


<!-- CORE SCRIPTS-->
<script src="{{ asset('assets/admin/js/app.min.js') }}" type="text/javascript"></script>

@yield('scripts')
<script>
    setTimeout(function () {
        $('.alert').slideUp();
    }, 6000);
</script>
</body>

</html>
