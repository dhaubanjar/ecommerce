@extends('admin.layouts.admin')
@section('content')

  <div class="page-content fade-in-up">

    <div class="row">
      <div class="col-lg-12">
        <div class="ibox">
          <div class="ibox-head">
            <div class="ibox-title">Product List</div>
            <div class="ibox-tools">
              <a href="{{route('products.create')}}" class="btn btn-primary"><i class="fa fa-plus">Add New</i> </a>
            </div>
          </div>
          <div class="ibox-body">
            <table class="table table-striped table-hover">
              <thead>
              <tr>
                <th>S.N</th>
                <th>Title</th>
                <th>Thumbnail</th>
                <th>Slug</th>
                <th>Summary</th>
                <th>Is Parent?</th>
                <th>Parent</th>
                <th>Status</th>
                <th>Action</th>
              </tr>
              </thead>
              <tbody>
              @if(isset($product_list))
                @foreach($product_list as $key => $value)
                  <tr>
                    <td>{{$key + 1}}</td>
                    <td>{{$value->title}}</td>
                    <td>
                      @if($value->image != null && file_exists(public_path().'/uploads/product/Thumb-'.$value->image))
                        <img src="{{asset('/uploads/product/Thumb-').$value->image}}" alt="" class="img img-responsive img-thumbnail">
                      @endif
                    </td>
                    <td>{{$value->slug}}</td>
                    <td>
                      <a href="{{$value->summary}}" target="_product">{{$value->summary}}</a>
                    </td>
                    <td>{{$value->is_parent == 1 ? "Yes" : "No"}}</td>
                    <td>{{$value->parent_id == 0 ? "N/a" : $value->parent_info['title']}}</td>
                    <td>{{$value->status}}</td>
                    <td>
                      <a href="{{route('product.edit', $value->id)}}" class="btn btn-success" style="border-radius: 50%; margin-bottom: 2px"><i class="fa fa-pencil"></i>
                      </a>

                      {{ Form::open(['url' => route('product.destroy', $value->id), 'class' => 'form', 'method'=>'delete', 'onsubmit' => "return confirm('Are you sure you want to delete this product?')"]) }}
                      {{ Form::button("<i class='fa fa-trash'></i>",['class' => 'btn btn-danger', 'style' => 'border-radius:50%', 'type' => 'submit']) }}
                      {{ Form::close() }}
                    </td>
                  </tr>
                @endforeach
              @endif
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>


  </div>
@endsection
